import os
import nrrd

import numpy as np
import __projections as proj

from matplotlib import pyplot as plt


def main(file_path=os.path.join('files', 'example.nrrd')):
    content, info = nrrd.read(file_path)
    list_imitation = list(np.swapaxes(content,2,0))

    ax_aspect, sag_aspect, cor_aspect = proj.get_display_aspects_nrrd(info)
    img3d = proj.list_to_3d_array(list_imitation)
    
    __diplay_matplotlib(ax_aspect, sag_aspect, cor_aspect, content) # в nrrd сразу лежит 3d массив
    __diplay_matplotlib(ax_aspect, sag_aspect, cor_aspect, img3d) # а тут мы имитируем получение 3d массива из списка изображений (например дайком)


def __diplay_matplotlib(ax_aspect, sag_aspect, cor_aspect, img3d, slice_nums = (0, 25, 40)):
    a1 = plt.subplot(2, 2, 1)
    plt.imshow(img3d[:, :, slice_nums[0]])
    a1.set_aspect(ax_aspect)

    a2 = plt.subplot(2, 2, 2)
    plt.imshow(img3d[:, slice_nums[1], :])
    a2.set_aspect(sag_aspect)

    a3 = plt.subplot(2, 2, 3)
    plt.imshow(img3d[slice_nums[2], :, :].T)
    a3.set_aspect(cor_aspect)

    plt.show()


if __name__ == '__main__':
    main()