
import numpy as np


def list_to_3d_array(raw_data):
    img_shape = list(raw_data[0].shape)
    img_shape.append(len(raw_data))
    img3d = np.zeros(img_shape)

    for idx, slice in enumerate(raw_data):
        img3d[:, :, idx] = slice
    return img3d

def get_display_aspects_nrrd(info):
    pixel_x_dim = info['space directions'][0][0]
    pixel_y_dim = info['space directions'][1][1]
    slice_thickness = info['space directions'][2][2]
    
    ax_aspect = pixel_y_dim / pixel_x_dim
    sag_aspect = pixel_y_dim / slice_thickness
    cor_aspect = slice_thickness / pixel_x_dim
    return ax_aspect,sag_aspect,cor_aspect